resource "hcloud_server" "managers" {
  count       = "${var.swarm_manager_nodes}"
  name        = "manager${count.index + 1}"
  location    = "${var.location}"
  server_type = "${var.swarm_manager_instance_type}"
  image       = "ubuntu-18.04"
  ssh_keys    = "${var.ssh_key_ids}"
  keep_disk   = true
}

resource "hcloud_server" "workers" {
  count       = "${var.swarm_worker_nodes}"
  name        = "worker${count.index + 1}"
  location    = "${var.location}"
  server_type = "${var.swarm_worker_instance_type}"
  image       = "ubuntu-18.04"
  ssh_keys    = "${var.ssh_key_ids}"
  keep_disk   = true

  depends_on = ["hcloud_server.managers"]
}

resource "hcloud_server" "runner" {
  count       = "${var.swarm_runner_nodes}"
  name        = "runner${count.index + 1}"
  location    = "${var.location}"
  server_type = "${var.swarm_runner_instance_type}"
  image       = "ubuntu-18.04"
  ssh_keys    = "${var.ssh_key_ids}"
  keep_disk   = true

  depends_on = ["hcloud_server.managers"]
}

resource "hcloud_server" "gluster" {
  count       = "${var.gluster_nodes}"
  name        = "gluster${count.index + 1}"
  location    = "${var.location}"
  server_type = "${var.gluster_instance_type}"
  image       = "ubuntu-18.04"
  ssh_keys    = "${var.ssh_key_ids}"
  keep_disk   = true
}

resource "hcloud_volume" "gluster_volumes" {
  count     = "${var.gluster_nodes}"
  name      = "gluster${count.index + 1}"
  size      = 10
  server_id = "${element(hcloud_server.gluster, count.index).id}"
  automount = false
  format    = "xfs"

  depends_on = ["hcloud_server.gluster"]
}
