output "swarm_manager_addrs" {
  value = "${hcloud_server.managers.*.ipv4_address}"
}

output "swarm_worker_addrs" {
  value = "${hcloud_server.workers.*.ipv4_address}"
}

output "swarm_runner_addrs" {
  value = "${hcloud_server.runner.*.ipv4_address}"
}

output "gluster_node_addrs" {
  value = "${hcloud_server.gluster.*.ipv4_address}"
}

output "domains" {
  value = ["${cloudflare_record.cluster_managers.*.hostname}"]
}
