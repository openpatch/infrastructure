FROM ubuntu:18.10

RUN apt update \
  && apt install -y software-properties-common \
  && apt-add-repository ppa:ansible/ansible \
  && apt install -y wget unzip ansible

RUN wget https://releases.hashicorp.com/terraform/0.12.1/terraform_0.12.1_linux_amd64.zip \
  && unzip terraform_0.12.1_linux_amd64.zip \
  && mv terraform /usr/local/bin

RUN apt remove -y wget unzip software-properties-common \
  && rm -rf terraform_0.12.1_linux_amd64.zip \
  && rm -rf /var/lib/apt/lists/*
