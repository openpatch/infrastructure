[ -z "$TF_BACKEND_aws_bucket" ] && export TF_BACKEND_aws_bucket="openpatch-infrastructure";
[ -z "$TF_BACKEND_aws_key" ] && export TF_BACKEND_aws_key="testing"
[ -z "$TF_BACKEND_aws_region" ] && export TF_BACKEND_aws_region="eu-central-1"

terraform init \
  -backend-config="bucket=$TF_BACKEND_aws_bucket" \
  -backend-config="key=$TF_BACKEND_aws_key" \
  -backend-config="region=$TF_BACKEND_aws_region"
