data "template_file" "inventory" {
  template = "${file("templates/inventory.tpl")}"

  depends_on = [
    "hcloud_server.managers",
    "hcloud_server.workers",
    "hcloud_server.runner",
    "hcloud_server.gluster"
  ]

  vars = {
    managers = "${join("\n", hcloud_server.managers.*.ipv4_address)}"
    workers  = "${join("\n", hcloud_server.workers.*.ipv4_address)}"
    runner   = "${join("\n", hcloud_server.runner.*.ipv4_address)}"
    gluster  = "${join("\n", hcloud_server.gluster.*.ipv4_address)}"
  }
}

resource "null_resource" "cmd" {
  triggers = {
    template_rendered = "${data.template_file.inventory.rendered}"
  }

  provisioner "local-exec" {
    command = "echo '${data.template_file.inventory.rendered}' > ../ansible/inventory"
  }
}
