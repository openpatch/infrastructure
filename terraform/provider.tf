provider "hcloud" {
  version = "~> 1.0"
  token   = "${var.hcloud_token}"
}

provider "cloudflare" {
  version = "~> 1.0"
  email   = "${var.cloudflare_email}"
  token   = "${var.cloudflare_token}"
}
