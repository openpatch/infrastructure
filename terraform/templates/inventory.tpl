[managers]
${managers}

[workers]
${workers}

[runner]
${runner}

[gluster]
${gluster}

[all:vars]
ansible_python_interpreter=/usr/bin/python3
