provider "netlify" {
  version = "~> 0.3"
  token   = "${var.netlify_token}"
}

provider "gitlab" {
  version = "~> 2.0"
  token   = "${var.gitlab_token}"
}
