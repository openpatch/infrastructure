import os
import json
from locust import HttpLocust, TaskSet, task

USER = os.getenv("USER")
PASSWORD = os.getenv("PASSWORD")


class CodeEvaluateBehavior(TaskSet):
    def on_start(self):
        self.login()

    @task(1)
    def evaluate_choice(self):
        tasks_json = [
            {
                "data": {"allow_multiple": True, "choices": ["Blue", "Teal", "Orange"]},
                "evaluation": {"choices": {"1": True}, "skip": False},
                "format_type": "choice",
                "format_version": 1,
                "task": "What is blue?",
            }
        ]

        solutions_json = {"0": {"value": ["1"]}}

        r = self.client.post(
            "/itembank/v1/items/versions/evaluate",
            json={"tasks": tasks_json, "solutions": solutions_json},
            headers=self.headers,
        )

    def login(self):
        r = self.client.post(
            "/authentification/v1/login", json={"username": USER, "password": PASSWORD}
        )
        self.jwt = r.json().get("access_token")
        self.refresh_token = r.json().get("refresh_token")
        self.headers = {"Authorization": "Bearer %s" % self.jwt}


class WebsiteItem(HttpLocust):
    task_set = CodeEvaluateBehavior
    min_wait = 5000
    max_wait = 9000
