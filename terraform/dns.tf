resource "cloudflare_record" "cluster_managers" {
  depends_on = ["hcloud_server.managers"]

  count = "${var.swarm_manager_nodes}"

  domain  = "${var.domain}"
  name    = "${element(hcloud_server.managers, count.index).name}"
  value   = "${element(hcloud_server.managers, count.index).ipv4_address}"
  type    = "A"
  proxied = true
}

resource "cloudflare_record" "cluster_root" {
  depends_on = ["hcloud_server.managers"]

  domain  = "${var.domain}"
  name    = "api"
  value   = "${hcloud_server.managers.0.ipv4_address}"
  type    = "A"
  proxied = true
}

resource "cloudflare_record" "cluster_short_root" {
  depends_on = ["hcloud_server.managers"]

  domain  = "${var.short_domain}"
  name    = "${var.short_domain}"
  value   = "${hcloud_server.managers.0.ipv4_address}"
  type    = "A"
  proxied = true
}

resource "cloudflare_record" "status_web" {
  domain  = "${var.domain}"
  name    = "status"
  value   = "stats.uptimerobot.com"
  type    = "CNAME"
  proxied = false
}

resource "cloudflare_record" "stats" {
  domain  = "${var.domain}"
  name    = "stats"
  value   = "custom.plausible.io"
  type    = "CNAME"
  proxied = false
}

resource "cloudflare_record" "next_web" {
  domain  = "${var.domain}"
  name    = "next"
  value   = "alias.zeit.co"
  type    = "CNAME"
  proxied = false
}

resource "cloudflare_record" "beta_web" {
  domain  = "${var.domain}"
  name    = "beta"
  value   = "alias.zeit.co"
  type    = "CNAME"
  proxied = false
}

resource "cloudflare_record" "files" {
  domain  = "${var.domain}"
  name    = "files"
  value   = "f002.backblazeb2.com"
  type    = "CNAME"
  proxied = true
}

resource "cloudflare_page_rule" "files_cache" {
  zone   = "openpatch.app"
  target = "https://files.${var.domain}/file/openpatch-static/*"
  actions {
    cache_level = "aggressive"
  }
}

resource "cloudflare_page_rule" "files_redirect" {
  zone   = "openpatch.app"
  target = "https://files.${var.domain}/file/*/*"
  actions {
    forwarding_url {
      url         = "https://secure.backblaze.com/404notfound"
      status_code = 302
    }
  }
}


resource "cloudflare_record" "og" {
  domain  = "${var.domain}"
  name    = "og"
  value   = "alias.zeit.co"
  type    = "CNAME"
  proxied = false
}

resource "cloudflare_record" "web" {
  domain  = "${var.domain}"
  name    = "@"
  value   = "alias.zeit.co"
  type    = "CNAME"
  proxied = false
}

resource "cloudflare_record" "www_web" {
  domain  = "${var.domain}"
  name    = "www"
  value   = "alias.zeit.co"
  type    = "CNAME"
  proxied = false
}

resource "cloudflare_record" "wildcard" {
  depends_on = ["cloudflare_record.cluster_root"]

  domain  = "${var.domain}"
  name    = "*.api"
  value   = "api.${var.domain}"
  type    = "CNAME"
  proxied = false
}
