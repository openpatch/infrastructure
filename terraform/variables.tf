variable "gitlab_token" {
  description = "Gitlab token"
}

variable "netlify_token" {
  description = "Netlify token"
}

variable "cloudflare_email" {
  description = "Cloudflare email"
}

variable "cloudflare_token" {
  description = "Cloudflare token"
}

variable "domain" {
  description = "Domain"
  default     = "openpatch.app"
}

variable "short_domain" {
  description = "Short Domain"
  default     = "opsl.app"
}

variable "hcloud_token" {
  description = "HCloud token"
}
variable "location" {
  description = "Location of server"
  default     = "nbg1"
}

variable "ssh_key_ids" {
  description = "SSH key ids"
  type        = list(string)
  default     = ["openpatch-infrastructure"]
}

variable "swarm_manager_nodes" {
  description = "Number of swarm managers"
  default     = 1
}

variable "swarm_manager_instance_type" {
  description = "Instance type"
  default     = "cx21-ceph"
}

variable "swarm_worker_nodes" {
  description = "Number of swarm workers"
  default     = 0
}

variable "swarm_worker_instance_type" {
  description = "Instance type"
  default     = "cx21-ceph"
}

variable "swarm_runner_nodes" {
  description = "Number of swarm runner"
  default     = 1
}

variable "swarm_runner_instance_type" {
  description = "Instance type"
  default     = "cpx11"
}

variable "gluster_nodes" {
  description = "Number of gluster nodes"
  default     = 0
}

variable "gluster_instance_type" {
  description = "Instance type"
  default     = "cx11-ceph"
}
